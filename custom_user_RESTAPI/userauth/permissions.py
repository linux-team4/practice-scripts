from rest_framework.permissions import BasePermission

class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == 'admin'

class IsStaff(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == 'staff' and request.method == 'GET'

class IsUser(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == 'user'

    