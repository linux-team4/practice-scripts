from django.shortcuts import render
from rest_framework.response import Response
from .serializers import UserSerializer,BlogSerializer
from .models import User,Blog
from rest_framework.generics import ListCreateAPIView
from .permissions import IsUser, IsAdmin, IsStaff
from rest_framework.views import APIView

# Create your views here.

class Base1(APIView):
    permission_classes = [IsUser]

class Base2(APIView):
    permission_classes = [IsAdmin]

class Base3(APIView):
    permission_classes = [IsStaff]

class UserList(Base2, ListCreateAPIView):  #admin only view this one 
    serializer_class = UserSerializer
    queryset = User.objects.all()

class BlogList(Base3, ListCreateAPIView):  #staff only view this one
    serializer_class = BlogSerializer
    queryset = Blog.objects.all()
