from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.

class User(AbstractUser):

    CHOICES = (

        ('admin', 'admin'),
        ('staff', 'staff'),
        ('user', 'user'),

    )

    role = models.CharField(max_length=10, choices=CHOICES)
    age = models.IntegerField(default=0)

    def __str__(self):
        return self.username

class Blog(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
   

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Blog"
