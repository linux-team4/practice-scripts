from django.urls import path
from . import views

urlpatterns = [
   
   path('',views.SearchView.as_view(),name='SearchView'),
   path('login/',views.login_view,name='login_view'),
   path('logout/',views.logout_view,name='logout_view'),
]
