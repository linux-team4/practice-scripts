from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView
from .models import Search
from .serializers import SearchSerializer
from rest_framework import filters
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from django.contrib.auth import login,logout,authenticate
from rest_framework.decorators import api_view

# Create your views here.

class SearchView(ListCreateAPIView):
    queryset = Search.objects.all()
    serializer_class = SearchSerializer
    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]
    filterset_fields = ['search']
    search_fields = ['search']
    ordering_fields = ['search']


@api_view(['POST'])
def login_view(request):
    
    username = request.data['username']
    password = request.data['password']

    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return Response({'message': 'Login successful'})
    else:
        return Response({'message': 'Invalid credentials'})


@api_view(['POST'])
def logout_view(request):
    logout(request)
    return Response({'message': 'Logout successful'})   
