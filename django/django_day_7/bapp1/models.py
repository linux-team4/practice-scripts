from django.db import models

# Create your models here.

class Employee(models.Model):

    name = models.CharField(max_length=20)
    email =models.EmailField()
    contact =models.IntegerField()
    address =models.TextField()  

    def __str__(self):
        return f'message from {self.name}'

