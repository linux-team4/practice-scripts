from django.shortcuts import render
from .models import Employee


# Create your views here.

def homepage(request):

    employee = Employee.objects.all()
    return render(request,'homepage.html',{"user":employee})