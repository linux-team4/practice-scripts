from django.apps import AppConfig


class Bapp9Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp9'
