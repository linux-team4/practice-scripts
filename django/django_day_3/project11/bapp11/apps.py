from django.apps import AppConfig


class Bapp11Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp11'
