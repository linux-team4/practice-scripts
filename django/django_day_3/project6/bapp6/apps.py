from django.apps import AppConfig


class Bapp6Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp6'
