from django.urls import path
from bapp1 import views

urlpatterns = [
    path('',views.homepage,name="homepage"),
    path('login/',views.userlogin,name="login"),
    path('register/',views.userregister,name="register"),

]
