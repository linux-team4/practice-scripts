from django.shortcuts import render

# Create your views here.

def homepage(request):
    return render(request,'homepage.html')

def userlogin(request):
    return render(request,'login.html')

def userregister(request):
    return render(request,'register.html')
