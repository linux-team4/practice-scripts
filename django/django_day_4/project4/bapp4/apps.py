from django.apps import AppConfig


class Bapp4Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp4'
