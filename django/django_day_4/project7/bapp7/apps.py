from django.apps import AppConfig


class Bapp7Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp7'
