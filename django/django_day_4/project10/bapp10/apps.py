from django.apps import AppConfig


class Bapp10Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp10'
