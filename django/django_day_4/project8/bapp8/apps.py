from django.apps import AppConfig


class Bapp8Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp8'
