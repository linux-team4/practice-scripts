from django.apps import AppConfig


class Bapp5Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bapp5'
