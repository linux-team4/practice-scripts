from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

# def homepage(request):
#     return HttpResponse('homepage')

def homepage(request):
    return render(request,'homepage.html')

def login(request):
    return render(request,'login.html')

def register(request):
    return render(request,'register.html')

def contact(request):
    return render(request,'contact.html')