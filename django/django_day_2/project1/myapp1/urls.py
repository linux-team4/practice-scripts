from django.urls import path
from myapp1 import views

urlpatterns = [
    
    path('',views.homepage,name='homepage'),
    path('login/',views.login,name='login'),
    path('register/',views.register,name='register'),
    path('contact/',views.contact,name='contact'),
]


# path('',views.functionname,name='homepage'),
# path('',views.classname.as_views(),name='homepage'),

