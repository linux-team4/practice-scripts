from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response

# from django.contrib.auth.decorators import login_required

# Create your views here.


@api_view(['POST'])

def logout(req):
    if req.method == 'POST':
        req.user.auth_token.delete()
        print('************user************',req.user)
        return Response({'msg':'logout successfully'})
       
    else:
        return Response({'msg':'logout failed'})
