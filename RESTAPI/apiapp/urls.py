from django.urls import path
from . import views


urlpatterns = [
    path('',views.index,name="index"),
    path('crud/<int:id>',views.crud,name="crud"),

]
