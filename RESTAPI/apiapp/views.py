from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import Detail
from .serializer import DetailSerializer

@api_view(['GET','POST'])
def index(request):
    if request.method == 'POST':
        serializer = DetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
    
    elif request.method =='GET':
         obj = Detail.objects.all()
         serializer = DetailSerializer(obj,many=True)
         return Response(serializer.data)
    
@api_view(['GET','PUT','DELETE'])    
def crud(request,id):
    if request.method == 'GET':
        obj = Detail.objects.get(id=id)
        serializer = DetailSerializer(obj)
        return Response(serializer.data)
        


    elif request.method == 'PUT':
        obj = Detail.objects.get(id=id)
        serializer = DetailSerializer(obj,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors)
        
    
    else:
        obj = Detail.objects.get(id=id)
        obj.delete()
        return Response({'message':'person is deleted'})
    


   
        


