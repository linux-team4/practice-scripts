from django.db import models

# Create your models here.

class Detail(models.Model):
    name = models.CharField(max_length=20)
    age = models.IntegerField()
    dept = models.CharField(max_length=30)
    date = models.DateField()
