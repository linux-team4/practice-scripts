def addition(n):
    return n+n

num = (1,2,3,4,5,6)
res = map(addition,num)
print(list(res))

# lambda and map combiantion

num = (1,2,3,4,5,6)
res = map(lambda x : x*x,num)
print(list(res))

num = (1,2,3,4,5,6)
num1= (2,52,96,24,25,56)
res = map(lambda x,y : x*y,num,num1)
print(list(res))
#<------------------------------------>
def bala(num): #map function
    if num%2==0:
        return "even"
    else:
        return "odd"
    
number = (1,2,3,4,5,6)
res = map(bala,number)
print(list(res))

#<------------------------------------>

li = [1,2,3,4,5,6]     #using lambda and map function
res = map(lambda x : x%2==0,li)
print(list(res))




