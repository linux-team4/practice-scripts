             #directly we cant access this method in python so we use some condition
             # Methode overloading................


class A :
    def fun(self,a=None,b=None,c=None):
        if(a!=None and b!=None and c!=None):
            print(a+b+c)
        elif(a!=None and b!=None):
            print(a+b)
        else:
            print(a)
obj = A()
obj.fun(10)
obj.fun(10,20,40)
obj.fun(10,50)

  # another method fo overloading
class a :
    def fun1(self,*bala):
        count = 0
        for i in bala:
            count=count+i
        print("added value :",count)
obj = a()
obj.fun1(10,20,30,42,48)
obj.fun1(23,56,84)

  # to find sq area and rec area

class area:
    def fun(self,a=None,b=None):
        if(a!=None and b!=None and a!=b):
            print("rec value :",a*b)
        else:
            print("sqr value :",a*a)
obj = area()
obj.fun(20)
obj.fun(30,40)
obj.fun(30,30)

 # multiple dispatch method its a module in python

from multipledispatch import dispatch

@dispatch(int,int)
def add(a,b):
    return a+b

@dispatch(int,int,int)
def add(a,b,c):
    return a+b+c

print(add(10,20))
print(add(10,20,30))

#.............................................

class dispatch:

 @dispatch(int,int)
 def add(a,b):
    print(a+b)

 @dispatch(int,int,int)
 def add(a,b,c):
    print(a+b+c)
 
 @dispatch(str,str)
 def add(a,b):
    print(a+b)

 @dispatch(float,float,int)
 def add(a,b,c):
    print(a*b+c)
 
 @dispatch(float,float)
 def add(a,b):
    print(a*b)

 @dispatch(int,float)
 def add(a,b):
    print(a*b)
obj = dispatch()
obj.add("happy","birthday")
obj.add(25,25)
obj.add(25,25,50)
obj.add(2.5,2.5,23)
obj.add(52.6,96.5)
obj.add(1001,23.5)
      
