def bala(num): #filter function
    if num%2==0:
        return True
    else:
        return False
    
number = (1,2,3,4,5,6)
res = filter(bala,number)
print(list(res))

li = [1,2,3,4,5,6]     #using lambda and filter function
res = filter(lambda x : x%2==0,li)
print(list(res))

res = filter(lambda x : x%2!=0,range(0,11))
print(list(res))

res = filter(lambda x : x%2==0,range(0,11))
print(list(res))                        
