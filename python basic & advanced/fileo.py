import os
import shutil

path = input("Enter Path: ")
files = os.listdir(path)

for file in files:
    filename, extension = os.path.splitext(file)#p.py
    extension = extension[1:]
    print(extension)

    if os.path.exists(path+'/'+extension):#home/py
        shutil.move(path+'/'+file, path+'/'+extension+'/'+file)
    else:
        os.makedirs(path+'/'+extension)#home/py
        shutil.move(path+'/'+file, path+'/'+extension+'/'+file)
        #home/p.py,home/py/p.py