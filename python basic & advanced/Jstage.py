from bs4 import BeautifulSoup
import requests
import gzip

input_file_path = 'output/html/jstage/https:--www.jstage.jst.go.jp-article-jamdsm-1-1-1_1_1-_article--char-en.html.gz'
with gzip.open(input_file_path, 'rb') as f:
    html_content = f.read()
    soup = BeautifulSoup(html_content, 'lxml')
    # print(soup)
title = {}
titles = soup.find('meta',attrs={"name":"title"})
title_meta = titles['content']
title['title'] = title_meta
print(title)

abstract = {}
abstract_text = soup.find('meta',attrs={"name":"abstract"})['content']
abstract['abstract_text'] = abstract_text
print(abstract)

keywords = {}
keywords_div = soup.find('div', class_='global-para')
keywords_text = "".join(keyword.strip() for keyword in keywords_div.stripped_strings)[9:]
keywords['keywords'] = keywords_text
print(keywords)

access_control = {}
access_text = soup.find('meta', attrs={"name":"access_control"})['content']
access_control['access_type'] = access_text
print(access_control)

journal_issn = {}
issn = soup.find('meta',attrs={"name":"citation_issn"})['content']
journal_issn['jounal_issn'] = issn
print(journal_issn)

doi_details = {}
doi = soup.find('meta',attrs={"name":"citation_doi"})['content']
doi_details['doi'] = doi
print(doi_details)

pdf_url = {}
pdf = soup.find('meta',attrs={"name":"citation_pdf_url"})['content']
pdf_url['pdf_url'] = pdf
print(pdf_url)

html_url = {}
html = soup.find('meta',attrs={"name":"og:url"})['content']
html_url['html_url'] = html
print(html_url)

volume = {}
vol_ume = soup.find('meta',attrs={"name":"volume"})['content']
volume['volume'] = vol_ume
print(volume)

issue = {}
is_sue = soup.find('meta',attrs={"name":"issue"})['content']
issue['issue'] = is_sue
print(issue)

pages = {}
pa_ges = soup.find('p',class_="global-para").text
pages_text = pa_ges.split()[-1]
pages['pages_num'] = pages_text
print(pages)

journalpublisher = {}
jp = soup.find('meta',attrs={"name":"publisher"})['content']
journalpublisher['publisher'] = jp
print(journalpublisher)

journal_abbre = {}
ab = soup.find('meta',attrs={"name":"journal_abbrev"})['content']
journal_abbre['abbreviation'] = ab
print(journal_abbre)

language = {}
ln = soup.find('meta',attrs={"name":"language"})['content']
language['language'] = ln
print(language)

on_issn = {}
on = soup.find('meta',attrs={"name":"online_issn"})['content']
on_issn['on_issn'] = on
print(on_issn)

author = {}
auth_ins = {}
for au in soup.find_all('meta',attrs={"name":"citation_author"}):
    ae = au['content']
    author['author'] = ae
    print(author)
for au_in in soup.find_all('meta',attrs={"name":"citation_author_institution"}):
    auin=au_in['content']
    auth_ins['institute'] = auin
    print(auth_ins)

received_date = {}
date = soup.find('meta',attrs={"name":"citation_publication_date"})['content']
received_date['Received_date'] = date
print(received_date)

published_date = {}
pb_date = soup.find('meta',attrs = {"name":"citation_online_date"})['content']
published_date['published_date'] = pb_date
print(published_date)












    