list = [1,2,3,"python","java"]
print(iter(list))

list = [1,2,3,"python","java"]
it = (iter(list))
print(next(it))
print(it.__next__())
print(next(it))
print(next(it))
print(next(it))


class ten:
    def __init__(self):
        self.num = 1
    def __iter__(self):
        return self
    def __next__(self):
        if self.num <= 10:
            values = self.num
            self.num +=1
            return values
        else:
            raise StopIteration
        
obj = ten()

for i in obj:
    print(i)

    #<-------------user defined------------------------>

def looping(value):
    it = iter(value)
    while True:
        try:
            item = next(it)
        except StopIteration:
            break
        else:
            print(item)

looping([1,2,3,4,5,6,7,8,9,10])




