def glo():
    loc = "python"
    print("inside call :",loc)
glo()

glo_var = "java"
def glo():
    loc = "python"
    print("inside call :",loc)
glo()
print("outside call :", glo_var)

glo_var = "java"
def glo():
      global loc
      loc = "python"
      print("inside call :",glo_var)
glo()
print("outside call :", loc)

glo_var = "java"
def glo():
      globals()['loc'] = "changed"
      loc = "python"
      print("inside call :",glo_var)
glo()
print("outside call :", loc)

def add(*y):
     z=0
     for i in y:
          z=i+z
     print("the sum is :", z)
add(10,20,30,40)
     

