class A :
    def add(self,name,age):     #private vaiables...............
        self.__name = name
        self.__age = age
        print("name is :", obj.__name)
        print("age is :", obj.__age)
obj = A()
obj.add("bala",24)

class students:               # public access specifier and call the fun 
    def details(self):
        self.name = "govt scl"
        print("school name :",self.name)

    def stu1(self):
        print("name : bala")
        print("cls : 10th")

    def stu2(self):
        print("name : partha")
        print("cls : 11th")

obj1=students()
obj1.details()
obj1.stu1()
obj1.stu2()

class students:               # private access specifier and call the fun 
    def details(self):             #by using another public method
        self.name = "govt scl"
        print("school name :",self.name)

    def __stu1(self):        # .............private method
        print("name : bala")
        print("cls : 10th")

    def stu2(self):     #..............public method
        self.__stu1()
        print("name : partha")
        print("cls : 11th")

obj1=students()
obj1.details()
obj1.stu2()

    # name mangling method..............calling private methods

class students:               
    def details(self):             
        self.name = "govt scl"
        print("school name :",self.name)

    def __stu1(self):        
        print("name : bala")
        print("cls : 10th")

    def __stu2(self):     
        print("name : partha")
        print("cls : 11th")

obj1=students()
obj1.details()
obj1._students__stu1()    #name mangling ..
obj1._students__stu2()    #name mangling..

   #syntax : objname._classname__private method name

   # protected   variable

class base:
    def __init__(self):
        self._name = "bala"

class derived(base):
    def __init__(self):
        super().__init__()
        print("from derived class :", self._name)
        self._name = "partha"
        print("modified :", self._name)

obj = derived()
obj1 = base()
print("accessing from obj :",obj._name)
print("acessing form obj 1 :" , obj1._name)

    #setter and getter

class details():
    def setname(self,n):
        self.__name = n
    def getname(self):
        return self.__name
    
    def setage(self,a):
        self.__age = a
    def getage(self):
        return self.__age
    
    def display(self):
        print("name is :",self.__name)#----------->obj.getname()
        print("age is:",self.__age)  #------------>obj.getage()

obj = details()
obj.setname("balachandran")
obj.setage(24)
obj.display()


    

        
