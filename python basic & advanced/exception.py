x = -1
if x < 0:
    raise Exception("no number is below zero")

print(".................................")

x = "balaviper"
if not type(x) is int:
    raise TypeError("only integers are allowed")
