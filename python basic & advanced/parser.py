# system level imports first
import sys
import os

# global package imports next
import gzip
import json
import requests
import logging

#from citation import Citation as citedata

# specific dependency modules next
from bs4 import BeautifulSoup
# from almamater.almamater import alma_out as mainfun
# from almamater.almamater import JsonAlmamater as cf
# from random_useragent.random_useragent import Randomize
# r_agent_agent = Randomize()
# rm_agent = r_agent_agent.random_agent('desktop', 'linux')
# agent = {"User-Agent": rm_agent}

# json/ssrn directory create method
if not os.path.exists("out/json/jstage/"):
    os.makedirs("out/json/jstage/")

#logging.basicConfig(filename="logs/ssrn.log", level=logging.DEBUG,
                    #format="%(asctime)s:%(levelname)s:%(message)s")


class Jstage:

    def doi(soup):
        pass


def jstage(url):
    soup = BeautifulSoup(url, 'lxml')
    print("soup", soup)


if __name__ == "_main_":
    '''
    this function invoked only when used as a standalone script
    otherwise Article class and it's methods and other functions
    can be imported and used like any other standared module.
    '''
    url_input = sys.argv[1]
    print("url_input :: ", url_input)

    if ".html.gz" in url_input:
        print("hai")
        with gzip.open(url_input, 'rb') as f:
            file_content = f.read()
            url = file_content.decode('utf-8')

    else:
        print("hello")
        files = requests.get(url_input, headers=agent, timeout=60)
        url = files.text

    result = jstage(url)