x = lambda a, b : a*b
print(x(5,6))

x = lambda a, b, c : a+b+c
print(x(10,10,20))

def lamb(n):
    return lambda a, : a/n
lamb1 = lamb(10)
print(lamb1(5))

def lamb(n):
    return lambda a, : a*n
lamb1 = lamb(10)
print(lamb1(5))

def lamb(n):
    return lambda a, : a*n
lamb1 = lamb(10)
lamb2 = lamb(12)
print(lamb1(5))
print(lamb2(10))


print("completed...........................")

msg = lambda name,m : print("hello world",name,m)
msg("bala","how are you")

# user defined lambda function

x = lambda a,b=20,c=30 : a+b+c
print(x(10))

x = lambda a,b=20,c=30 : a+b+c
print(x(10,10))

x = lambda *bala : sum(bala)
print(x(10,20,30,40,50,60,70,80,90,100))

print((lambda a,b : a*b)(20,30))

# getting sqr values by using lambda function


ls1 = [1,4,30,15,25,16]
ls2 = []

for i in ls1:
    sqr = lambda i : i**2
    ls2.append(sqr(i))

print(ls2)

# higher order lamba funtion

x = lambda a,fun : a + fun(a)
print(x(20,lambda a : a*a))


 