import gzip
from bs4 import BeautifulSoup
import requests

class JStage:
    def __init__(self):
        self.soup = None  # Initialize the soup object in the constructor

    def parsing_com_url(self):
        input_file_path = 'output/html/jstage/https:--www.jstage.jst.go.jp-article-jamdsm-1-1-1_1_1-_article--char-en.html.gz'
        with gzip.open(input_file_path, 'rb') as f:
            html_content = f.read()
            response = requests.get(html_content)
        self.soup = BeautifulSoup(response.content, 'lxml')
        
    def title(self):
        title = self.soup.find('div', class_="global-article-title").text
        print("title: " + title)

    def abstract(self):  # Added 'self' as a parameter for all class methods
        abstract = self.soup.find('p', class_="global-para-14").text
        print("abstract: " + abstract)

    def keywords(self):
        keywords = self.soup.find('div', class_="global-para").text
        print(keywords)

    def journal(self):
        journal = self.soup.find('span', class_="tags-wrap original-tag-style").text
        print(journal + " :Mechanical Engineering Journal")

    def accesstype(self):
        Access_type = self.soup.find('span', class_="tags-wrap freeaccess-tag-style").text
        print("access_type: " + Access_type)

    def authorinfo(self):
        Author_info = self.soup.find('div', class_="accordion_container").text
        print(Author_info)

    def pdflink(self):
        pdf_link = self.soup.find('div', class_="col-md-6 print-non-disp")
        a_tag = pdf_link.find('a')
        href = a_tag["href"]
        print(href)

    def details(self):
        details = self.soup.find_all('span', class_="accodion_lic")
        for span in details:
          all = span.text.strip()
          print(all)

if __name__ == "__main__":
    result = JStage()
    result.parsing_com_url()
    result.title()
    result.abstract()
    result.keywords()
    result.journal()
    result.accesstype()
    result.authorinfo()
    result.pdflink
    result.details()
