def fun():
    yield 5
obj = fun()
print(next(obj))

#<---------------------->

def fun():
    yield 5
    yield 4
    yield 3
    yield 2
    yield 1
obj = fun()
print(next(obj))
print(next(obj))
print(next(obj))
print(obj.__next__)
print(next(obj))
print(next(obj))

#<--------------------->

def fun():
    yield 5
    yield 4
    yield 3
    yield 2
    yield 1
obj = fun()

for i in obj:
    print(i)

#<---------------------->

def fun():
    yield 5
    yield 4
    yield 3
    yield 2
    yield 1


for i in fun():
    print(i)

#<------------------------>

def depth():
    str1 = "bala"
    yield str1

    str2 = "partha"
    yield str2

    str3 = "baskaransanthi"
    yield str3



for i in depth():
    print(i)

    #<---------------------->

def even():
    for i in range(10):
        if i%2==0:
            yield i
for i in even():
    print(i)

    #<---------------------->

def odd():
    for i in range(10):
        if i%2==1:
            yield i
for i in odd():
    print(i)

   #<----------------------->

def even():
    n=1
    while n<= 10:
        if n%2==1: 
         yield n
        n=n+1
for i in even():
    print(i)

    #<-------------------->

def even():
    n=1
    while n<= 10:
        if n%2==0: 
         yield n
        n=n+1
for i in even():
    print(i)

    #<----------------------->

def sqr():
    n=1
    while n<= 10:
         sq = n*n
         yield sq
         n=n+1
for i in sqr():
    print(i)