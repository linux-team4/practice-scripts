class demo:
    def __init__(self):
        self._age = 0
    @property
    def age(self):
     print("getter method is called")
     return self._age
    @age.setter
    def age(self,a):
     if a<18:
        print("you are below 18")
     print("setter method called")
     self._age=a

obj = demo()
obj.age = 20
print(obj.age)

class demo:
    def __init__(self):
        self._age = 0
    @property
    def age(self):
     print("getter method is called")
     return self._age
    @age.setter
    def age(self,a):
     if a<18:
        print("you are below 18")
     print("setter method called")
     self._age=a

obj = demo()
obj.age = 10
print(obj.age)
        