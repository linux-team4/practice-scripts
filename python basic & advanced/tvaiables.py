 # intance variable exm.

class A:
    def fun1(self,name,no):
        self.name = name 
        self.no = no
        print("name :",self.name)
        print("roll no:",self.no)

name = A()
name.fun1("bala",23654)

name1 = A()
name1.fun1("partha",33556)

 # class or static variable 

class B:       # class variable using class name . var name
     
     clg_name = "paavai engineering college"
     def __init__(self,name,no):
         self.name=name
         self.no = no
clg = B("bala",36523)
print(clg.name,clg.no,B.clg_name)

class B:       # class variable using object name.var name
     
     clg_name = "paavai engineering college"
     def __init__(self,name,no):
         self.name=name
         self.no = no
clg = B("bala",36523)
print(clg.name,clg.no,B.clg_name)
print(clg.clg_name)

class B:       # class variable accessing inside method name.var name
     
     clg_name = "paavai engineering college"
     def __init__(self,name,no):
         self.name=name
         self.no = no
         print(B.clg_name)
         print(self.clg_name)
clg = B("bala",36523)
print(clg.name,clg.no,B.clg_name)
print(clg.clg_name)

# modifying variable name with syntax cls name.var = ".."

class log:
    clg_var = "paavai engineering clg"
    def __init__(self,name,no):
        self.name = name 
        self.no = no
    def show(self):
        print(self.name,self.no,log.clg_var)

log.clg_var = "king clg"

s1 = log("bala",63565)
s1.show()

s2 = log("partha",65595)
s2.show()

        
     



         
        
    
