class person:
    def __init__(self):
        name = "bala"
        lname = "viper" 
        print("the name is : ",name)
        print("the last name is :",lname)
obj=person()
obj1 = person()

class person:
    def __init__(self):
        print("hai")
obj=person()

class person:
    def __init__(self):        #here override the function
        print("hello")
    def __init__(self):
        print("hai")
        
obj=person()
obj1=person()

class person:                 #parameterized constructor
    def __init__(self,name,age):
        print("name is :",name)
        print("age is :",age)
obj = person("bala",24)

class person:                 #parameterized constructor
    def __init__(self,name):
        age = 24
        print("name is :",name)
        print("age is :",age)
obj = person("bala")

class person:                 #parameterized constructor
    def __init__(self,name,age):
        self.name = name 
        self.age = age 
    def print(self):
        print("name is :",self.name)
        print("age is :",self.age)
obj = person("bala",24)
obj.print()

class person:                 #parameterized constructor
    def __init__(self,name,age):
        self.name = name 
        self.age = age 
    def print(self):
        print("name is :",self.name)
        print("age is :",self.age)
obj = person("bala",24)
obj = person("partha",23)
obj.print()

class person:                 #parameterized constructor
    def __init__(self,name,age):
        self.name = name 
        self.age = age 
    def print(self):
        print("name is :",self.name)
        print("age is :",self.age)
obj = person("bala",24)
obj.print()
obj1 = person("partha",23)
obj1.print()
obj2 = person("santhi",45)
obj2.print()
obj3 = person("baskaran",50)
obj3.print()
        
class person:                 #default value parameter
    def __init__(self,name="chandran",age = 24):
        self.name = name 
        self.age = age 
    def print(self):
        print("name is :",self.name)
        print("age is :",self.age)
obj = person()
obj.print()

class person:                 #default value parameter
    def __init__(self,name="chandran",age = 24):
        self.name = name 
        self.age = age 
    def print(self):
        print("name is :",self.name)
        print("age is :",self.age)
obj = person("partha",23)
obj.print()

class persons:
    def __init__(self,name,**values):
        print("name :",name)
        print("age :", values['age'])
        print("city :", values['city'])
        print("ph :", values['ph'])
obj = persons("balachandran",age=23,city="trichy",ph=6380688522)
        

        

    