se = {"apple","mango","orange","banana","cherry"}
print(se)

se = {"apple","mango","orange","banana","cherry"}
y = list(se)
y[0] = "doll" #index not working here due to unordered
se = set(y)
print(se)
print(type(se))

se = {"apple","mango","orange","banana","cherry","apple"}
print(se)

se = {"apple","mango","orange","banana","cherry"}
print(len(se))

se = {"apple","mango","orange","banana","cherry"}
for x in se:
    print(x)

se = {"apple","mango","orange","banana","cherry"}
print("banana" in se)

se = {"apple","mango","orange","banana","cherry"}
se.add("birds") #add values in set
print(se)

se = {"apple","mango","orange","banana","cherry"}
se1= {"apple","mango","orange","guava","cherry"}
se.update(se1) #combine values in set
print(se)

se = {"apple","mango","orange","banana","cherry"}
se.remove("apple")
print(se)

se = {"apple","mango","orange","banana","cherry"}
se.discard("mango")
print(se)

se = {"apple","mango","orange","banana","cherry"}
se.pop()
print(se)

se = {"apple","mango","orange","banana","cherry"}
se.clear()
print(se)

se = {"apple","mango","orange","banana","cherry"}
for x in se:
    print(x)

se = {"apple","mango","orange","banana","cherry"}
se1 = {"apple","orange","juggary",}
se3 = se.union(se1)
print(se3)

se = {"apple","mango","orange","banana","cherry"}
se1 = {"apple","orange","juggary",}
se.intersection_update(se1)
print(se)

se = {"apple","mango","orange","banana","cherry"}
se1 = {"apple","orange","juggary",}
print(se.symmetric_difference(se1))

se = {"apple","mango","orange","banana","cherry"}
se1 = {"apple","orange","juggary",}
print(se.symmetric_difference_update(se1))













