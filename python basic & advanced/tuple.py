x = ("apple","mango","orange")
print(x)

x = ("apple","mango","orange")
y = list(x)
y[0] = "banana"
x = tuple(y)
print(x)

x = ("apple","mango","orange")
y = list(x)
y.append("parrot")
x = tuple(y)
print(x)

x = ("apple","mango","orange")
y = ("cherry",)
x += y
print(x)

x = ("apple","mango","orange")
y = list(x)
y.remove("apple")
x= tuple(y)
print(x)

x = ("apple","mango","orange")
(green, yellow, blue) = x
print(green)
print(yellow)
print(blue)

x = ("apple","mango","orange","banana","cherry")
(green, yellow, *blue) = x
print(green)
print(yellow)
print(blue)

x = ("apple","mango","orange","banana","cherry")
(green, *yellow, blue) = x
print(green)
print(yellow)
print(blue)

x = ("apple","mango","orange","banana","cherry")
(green, yellow, *blue) = x
print(green)
print(yellow)
print(blue)

tu = ("apple","mango","orange","banana","cherry")
for x in tu:
    print(x)

tu = ("apple","mango","orange","banana","cherry")
for i in range(len(tu)):
    print(tu[i])

tu = ("apple","mango","orange","banana","cherry")
i=0
while i < len(tu):
    print(tu[i])
    i=i+1

tu = ("apple","mango","orange","banana","cherry")
tu1 = ("apple","mango","orange","banana","cherry")
tu3= tu + tu1
print(tu3)

tu = ("apple","mango","orange","banana","cherry")
tu1 = tu*2
print(tu1)

tu = ("apple","mango","orange","banana","cherry")
print(tu.count("apple"))




















