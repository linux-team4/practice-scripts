li = ["apple","banana","cherry"]       #single variable we can store multiple items
print(li)
li = ["apple","banana","cherry,apple"] # list allow duplicates , index basedone,changable
print(li)
li = ["apple","banana","cherry"]
print(len(li))

li = ["apple","banana","cherry"]
print(li)
li2 = [1,2,3,4,5,6,7] 
print(type(li2))                  #we can fill int,str and boolean values
print(li2)
li3 = [True,False]
print(li3)
li = ["apple",25,True,"orange"]
print(li)

li = list(("apple",256,True))          # list()............contuctor for creating list
print(li)
li = ["apple",25,True,"orange"]
print(li[2])
print(li[-2])
li = ["apple",25,True,"orange"]
print(li[1:3])
li = ["apple",25,True,"orange"]
if "apple" in li:                             #checking particular index value by using condition
    print("yes apple is in this list")
else:
    print("No apple not in this list")

li = ["apple",25,True,"orange"]
li[0] = "cherry"
print(li)
li = ["apple",25,True,"orange"]
li[0:3] = ["parrot","eagle","pigeon"]           #add the values in list for our expected index place
print(li)
li = ["apple",25,True,"orange"]
li.insert(1,"banana")                           # add the values in preferred index 
print(li)
li = ["apple",25,True,"orange"]
li.append("banana")                              # adding the single list value
print(li)
li = ["apple",25,True,"orange"]
li1 = ["apple","banana","orange"]
li.extend(li1)                                    #combine the two list values
print(li)
li1 = ["apple","banana","orange"]
li1.remove("apple")
print(li1)
li1 = ["apple","banana","orange"]
li1.pop()
print(li1)
li1 = ["apple","banana","orange"]
del li1[0]
print(li1)
li1 = ["apple","banana","orange"]
del li1
li1 = ["apple","banana","orange"]
li1.clear()
print(li1)
li1 = ["apple","banana","orange"]
for x in li1:
    print(x)

li1 = ["apple","banana","orange"]
for i in range(len(li1)):
    print(li1[i])

li = ["apple","banana","orange"]
i=0
while i < len(li):
    print(li1[i])
    i=i+1

li1 = ["apple","banana","orange"]
[print(x) for x in li1]

li1 = ["apple","banana","orange","cherry"]
li2 =[]
for x in li1:
    if "a" in li1:
        li2.append(x)
        print(li2)

li1 = ["apple","banana","orange","cherry","bala"]
li2 = [x for x in li1 if "a" in x]
print(li2)

li1 = ["apple","banana","orange"]
li2 =[x for x in li1]
print(li2)

li1 = ["apple","banana","orange"]
li2 = [x for x in li1 if x != "apple"]
print(li2)

li1 = ["apple","banana","orange"]
li2 = [x.upper() for x in li1]
print(li2)









