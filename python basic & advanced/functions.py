def include():
    print("hi bala")
include()

def name(fname):
    print(fname, "is your first name")
name("partha")
name("bala")

def name(fname,lname):
    print("Your full name is :", fname, " ", lname)
name("partha","sarathi")
name("bala","chandran")

def kidsname(*kids):
    print("the tallest kid is: ", kids[3])
kidsname("john","martin","tristen","karnan the beast")

def youngest(child1, child2, child3):
    print("The youngest child is ", child2)
youngest(child1="bala", child2="partha", child3="barkaran")

def name(**kids):
    print("the kids lname is :",kids["lname"])
name(fname = "bala", lname = "chandran")

def default(country="norway"):
    print("i am from :", country)
default()
default("india")
default("china")
default("russia")

def items(food):
    for x in food:
        print(x)

friuts = ["apple", "banana", "orange"]
items(friuts)

def returnvalue(x):
    return 5*x
print(returnvalue(3))
print(returnvalue(5))
print(returnvalue(8))

def empty():
    pass         #to avoid errors

def tryrecurtion(k):     #Recurtion
    if k>0:
        result = k + tryrecurtion(k -1)
        print(result)
    else:
        result=0
    return result
print("\n\n Recurtion example")
tryrecurtion(6)






