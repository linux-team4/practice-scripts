try:
    print(x)           #  x is not defined
except:
    print("an error occured")

try:
    print(x)
except NameError:
    print("variable is not defined")
except:
    print("something went wrong ")

try:
    print("hello")
except:
    print("something else went wrong")
else:
    print("nothing went wrong")

try:
    print(x)
except:
    print("something else went wrong")
finally:
    print("try block and except is finished")

    


