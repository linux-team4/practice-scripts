 #instance method------> using syntax cls name.method name(obj)

class A :
    def fun(self):
        print("im from method 1")
s = A()
A.fun(s)

class B :
    def show(self,a,b):
        print("added value:",a+b)
obj = B()
obj.show(20,60)

obj1 = B()
B.show(obj1,100,40)

# class method using decorator (cls.var)

class B :
    var = "class variable"
    def show(self,a,b):
        print("added value:",a+b)

    @classmethod
    def method2(cls):
        print("im from class method")
        print(cls.var)
obj = B()
obj.show(20,60)
B.show(obj,25,25)
B.method2()

# class method variable modified process

class B :
    var = "class variable"
    def show(self,a,b):
        print("added value:",a+b)

    @classmethod
    def method2(cls):
        print("im from class method")
        print(cls.var)
        cls.var = "modified"

    def show1(self):
        print(B.var)
obj = B()
obj.show(20,60)
B.show(obj,25,25)
B.method2()
B.show1(obj)

class myclass:
    def method(self):
        return "instance is called",self
    
    @classmethod
    def clsmethod(cls):
        return "cls method is called",cls
    
    @staticmethod
    def static_methode():
        return "static method is called"
    
obj = myclass()
print(obj.method())
print(myclass.method(obj))
print(obj.clsmethod())
print(myclass.clsmethod())
print(myclass.static_methode())
