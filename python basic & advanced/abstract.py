from abc import ABC,abstractmethod

class a(ABC):
    @abstractmethod
    def methode(self):
        pass   


# so we cant create object fo the abstract class ....>

from abc import ABC,abstractmethod

class a(ABC):
    @abstractmethod
    def method(self):
     print("im from cls a")

class b(a):
    def method(self):
        return super().method()
    
name = b()
name.method()