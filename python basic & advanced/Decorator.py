def outter():
    msg1 = "welcome home"
    def inner():
        msg2 = "bala"
        msg = msg1+msg2
        return msg
    return inner
obj = outter()
print(obj())

def outer(x):
    def inner(y):
        return x**y
    return inner
add = outer(5)
print(add(10))

def outter(name,age):
    def inner(gender):
        return name, age,gender
    return inner
pr_details = outter("bala",25)
print(pr_details("male"))
print(list(pr_details("male")))
for list1 in pr_details("male"):
    print(list1)
print(len(pr_details("male")))
print(type(list1))
print(type(pr_details("male")))

def fun1():
    print("im from fun 1")
def fun2():
    print("im from fun 2")
def fun3(ref,ref1):
    ref()
    ref1()
    
    print("im from fun 3")

fun3(fun1,fun2)

def addition(x,y):
    return x+y
def addition1(ref,x,y):
     return ref(x,y)

print(addition1(addition,25,25))

def upper(text):
    return text.upper()
def lower(text):
    return text.lower()
def text(res):
    return res("balaviper")
    

print(text(upper))
print(text(lower))

def fire(name):
    def inner():
        print ("hello" +name)
    return inner

rev = fire("bala")
rev()

def upperstring(ref):
    def process():
        data = ref()
        return data.upper()
    return process

@upperstring
def myfunction():
    return "happy learning"
print(myfunction())

#<----------------normal calling decoator-------------------------->

"""def upperstring(ref):
    def process1():
        data = ref()
        return data.upper()
    return process1
    

def upperstring2(ref):
    def process2():
        data = ref()
        return data.split()
    return process2

def myfunction():
    return "happy learning"

call =(upperstring2(upperstring(myfunction)))
print(call())
print(call)
"""
#<------------using @decorator calling------------------->

def upperstring(ref):
    def process1():
        data = ref()
        return data.upper()
    return process1
    

def upperstring2(ref):
    def process2():
        data = ref()
        return data.split()
    return process2
@upperstring2
@upperstring
def myfunction():
    return "happy learning"

print(myfunction())


#<---nested functions with decoator parameter passing

def outter(val):
    def decor1(ref):
        def process1():
            data = ref()
            return data.upper()+val
        return process1
    return decor1
@outter("BALAVIPER")
def myfunction():
    return "hello! welcome "

print(myfunction())

def outter(val):
    def decor1(ref):
        def process1():
            data = ref()
            return data.upper()+val.upper()
        return process1
    return decor1
name = input("enter the name :")
@outter(name)
def myfunction():
    return "hello! welcome "

print(myfunction())







    