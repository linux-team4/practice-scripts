class a:
    def bala(self):
        print("im from class a")

class b(a):
    def bala(self):
        super().bala()
        print("im from class b")

class c(b):
    def bala(self):
        super().bala()
        print("im from class c")

obj = c()
obj.bala()


  # method over riding ------> differnt cls same parameter

  # polymorphism using inheritance and method overriding

class bala:
    def family(self):
        return "bala"
    
class baskaran(bala):
    def family(self):
        return "baskaran"
    
class santhi(baskaran):
    def family(self):
        return "santhi"
    
class partha(santhi):
    def family(self):
        return "partha"
    
obj1 = bala()
obj2 = baskaran()
obj3 = santhi()
obj4= partha()

for i in (obj1,obj2,obj3,obj4):
    print(i.family())

    #...............................................................

class bala:
    def family(self):
        return "bala"
    
class baskaran(bala):
    def family(self):
        return "baskaran"
    
class santhi(baskaran):
    def family(self):
        return "santhi"
    
class partha(santhi):
    def family(self):
        return "partha"
    
obj = [bala(),baskaran(),santhi(),partha()]

for i in obj:
    print(i.family())



    

