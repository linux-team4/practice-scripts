dics = {"name" : "bala","Age"  : "24","year" :  1998}
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(dics["Age"])

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(len(dics))

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(type(dics))

dics = dict(name = "bala",age = 24,country = "tamilnadu")
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(dics.get("year"))

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
x = dics.keys()
dics["id"] = 262
print(x)
print(dics)
print(dics.keys())

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(dics.values())

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
x = dics.values()
print(x)
dics["year"] = 2000
print(x)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
print(dics.items())

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
if "Age" in dics:
    print("yes the age key is in this dictionary")
else:
    print("age is not present in the dictionary")

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
dics.update({"year" : 2021})
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
dics.pop("year")
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
dics.popitem()
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
del dics["name"]
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
dics.clear()
print(dics)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
for x in dics:
    print(x)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
for x in dics:
    print(dics[x])

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
for x in dics.keys():
    print(x)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
for x in dics.values():
    print(x)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
for x in dics.items():
    print(x)

dics = {"name" : "bala",
        "Age"  : "24",
        "year" :  1998}
dics1 = dics.copy()
print(dics1)

dics = {"no1":{"name" : "bala", # nested dictionary
        "Age"  : "24",
        "year" :  1998},
        "no2":{"name" : "bala",
        "Age"  : "24",
        "year" :  1998},
        "no3":{"name" : "bala",
        "Age"  : "24",
        "year" :  1998}}
print(dics)










