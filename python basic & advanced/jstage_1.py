from bs4 import BeautifulSoup
import requests
import gzip

class Jstage:

    def title(soup):
        title = {}
        titles = self.soup.find('meta',attrs={"name":"title"})
        title_meta = titles['content']
        title['title'] = title_meta
        print(title)

    def abstract(soup):
        abstract = {}
        abstract_text = self.soup.find('meta',attrs={"name":"abstract"})['content']
        abstract['abstract_text'] = abstract_text
        return abstract

    def keywords(soup):
        keywords = {}
        keywords_div = self.soup.find('div', class_='global-para')
        keywords_text = "".join(keyword.strip() for keyword in keywords_div.stripped_strings)[9:]
        keywords['keywords'] = keywords_text
        return keywords

    def access_type(soup):
        access_control = {}
        access_text = self.soup.find('meta', attrs={"name":"access_control"})['content']
        access_control['access_type'] = access_text
        return access_control

    def journal_issn(soup): 
        journal_issn = {}
        issn = self.soup.find('meta',attrs={"name":"citation_issn"})['content']
        journal_issn['jounal_issn'] = issn
        return journal_issn

    def doi(soup):
        doi_details = {}
        doi = self.soup.find('meta',attrs={"name":"citation_doi"})['content']
        doi_details['doi'] = doi
        return doi_details
    
    def pdf_link(soup):
        pdf_url = {}
        pdf = self.soup.find('meta',attrs={"name":"citation_pdf_url"})['content']
        pdf_url['pdf_url'] = pdf
        return pdf_url

    def html_link(soup)
        html_url = {}
        html = self.soup.find('meta',attrs={"name":"og:url"})['content']
        html_url['html_url'] = html
        return html_url

    def volume(soup):
        volume = {}
        vol_ume = self.soup.find('meta',attrs={"name":"volume"})['content']
        volume['volume'] = vol_ume
        return volume

    def issue(soup):
        issue = {}
        is_sue = self.soup.find('meta',attrs={"name":"issue"})['content']
        issue['issue'] = is_sue
        return issue

    def page_number(soup):
        pages = {}
        pa_ges = self.soup.find('p',class_="global-para").text
        pages_text = pa_ges.split()[-1]
        pages['pages_num'] = pages_text
        return pages

    '''def art_publisher(self):
        journalpublisher = {}
        jp = self.soup.find('meta',attrs={"name":"publisher"})['content']
        journalpublisher['publisher'] = jp
        return journalpublisher

    def art_abbre(self):
        journal_abbre = {}
        ab = self.soup.find('meta',attrs={"name":"journal_abbrev"})['content']
        journal_abbre['abbreviation'] = ab
        return journal_abbre

    def art_language(self):
        language = {}
        ln = self.soup.find('meta',attrs={"name":"language"})['content']
        language['language'] = ln
        return language'''
    
    def journal_issn(soup):
        on_issn = {}
        on = self.soup.find('meta',attrs={"name":"online_issn"})['content']
        on_issn['on_issn'] = on
        return on_issn

    def  author_institute(soup):
        author = {}
        auth_ins = {}
        for au in self.soup.find_all('meta',attrs={"name":"citation_author"}):
            ae = au['content']
            author['author'] = ae
            return author
        for au_in in self.soup.find_all('meta',attrs={"name":"citation_author_institution"}):
            auin=au_in['content']
            auth_ins['institute'] = auin
            return auth_ins

    def received_date(soup):
        received_date = {}
        date = self.soup.find('meta',attrs={"name":"citation_publication_date"})['content']
        received_date['Received_date'] = date
        return received_date

    def publication_date(soup):
        published_date = {}
        pb_date = self.soup.find('meta',attrs = {"name":"citation_online_date"})['content']
        published_date['published_date'] = pb_date
        return published_date

def jstage():
    
    soup = BeautifulSoup(url, 'lxml')
    doi  = Jstage.doi(soup)

    

    article={}
    article['pub_type']                = "Journal Article"
    article['access_type']             = Jstage.access_type(soup)
    article['journal']                 = Jstage.journal_title(soup)
    article['journal_issn']            = Jstage.journal_issn(soup)
    article['impact_factor']           = "0.0"
    article['doi']                     = Jstage.doi(soup)
    article['pmid']                    = ""
    article['pmc']                     = ""
    article['title']                   = Jstage.title(soup)
    article['authors']                 = Jstage.author_institute(soup)
    article['volume']                  = Jstage.volume(soup)
    article['issue']                   = Jstage.issue(soup)
    article['pagenum']                 = Jstage.page_number(soup)
    article['date_received']           = Jstage.received_date(soup)
    article['date_accepted']           = Jstage.publication_date(soup)
    article['date_published']          = Jstage.publication_date(soup)
    article['abstract']                = Jstage.abstract(soup)
    article['keywords']                = Jstage.keywords(soup)
    article['pdf_url']                 = Jstage.pdf_link(soup)
    article['html_url']                = Jstage.html_link(soup)

    return article

if __name__ == "__main__":
    '''
    this function invoked only when used as a standalone script
    otherwise Article class and it's methods and other functions
    can be imported and used like any other standared module.
    '''
    url_input= sys.argv[1]
    print(url_input)
    agent = {"User-Agent":'Mozilla/5.0  \
    (Windows NT 6.3; WOW64) AppleWebKit/537.36  \
    (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}
    urls = requests.get(url_input,headers=agent,timeout=60 )
    url=urls.text
    result = jstage(url)
    doi = result['doi']

    if len(doi) > 5:
        doi = doi.replace("/", "_")
        output = "out/json/jstage/" + doi + '.json'
        print("Writing...!", output)

        with open(output, 'w') as fp:
            json.dump(result, fp)

    else:
        pass




    