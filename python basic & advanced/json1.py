import gzip
from bs4 import BeautifulSoup
import requests
import json  

class JStage:
    def __init__(self):
        self.soup = None
        self.data = {}  
    def parsing_com_url(self):
        input_file_path = 'url_0.gz'
        with gzip.open(input_file_path, 'rb') as f:
            html_content = f.read()
            response = requests.get(html_content)
        self.soup = BeautifulSoup(response.text, 'lxml')

    def title(self):
        title = self.soup.find('div', class_="global-article-title").text
        self.data['title'] = title

    def abstract(self):
        abstract = self.soup.find('p', class_="global-para-14").text
        self.data['abstract'] = abstract

    def keywords(self):
        keywords = self.soup.find('div', class_="global-para").text
        self.data['keywords'] = keywords

    def journal(self):
        journal = self.soup.find('span', class_="tags-wrap original-tag-style").text
        self.data['journal'] = journal + " :Mechanical Engineering Journal"

    def accesstype(self):
        Access_type = self.soup.find('span', class_="tags-wrap freeaccess-tag-style").text
        self.data['access_type'] = Access_type

    def authorinfo(self):
        Author_info = self.soup.find('div', class_="accordion_container").text
        self.data['author_info'] = Author_info

    def pdflink(self):
        pdf_link = self.soup.find('div', class_="col-md-6 print-non-disp")
        a_tag = pdf_link.find('a')
        href = a_tag["href"]
        self.data['pdf_link'] = href

    def details(self):
        details = self.soup.find_all('span', class_="accodion_lic")
        detail_list = [span.text.strip() for span in details]
        self.data['details'] = detail_list

    def to_json(self):
        return json.dumps(self.data, indent=4)  

if __name__ == "__main__":
    result = JStage()
    result.parsing_com_url()
    result.title()
    result.abstract()
    result.keywords()
    result.journal()
    result.accesstype()
    result.authorinfo()
    result.pdflink()
    result.details()
    
    json_output = result.to_json()  
    print(json_output)
