import datetime

x = datetime.datetime.now()
print(x)
print(x.year)
print(x.strftime("%A"))
print(x.strftime("%a"))
print(x.strftime("%B"))
print(x.strftime("%b"))
print(x.strftime("%C"))
print(x.strftime("%c"))
print(x.strftime("%D"))
print(x.strftime("%d"))
print(x.day)
print(x.month)

x = datetime.datetime(1997,6,26)
print(x)

x = datetime.time(12,55,56)
print(x)
