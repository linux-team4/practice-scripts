from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view
from .models import Blog
from .serializers import BlogSerializer
from rest_framework.generics import ListAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.generics import RetrieveAPIView
from rest_framework.generics import UpdateAPIView
from rest_framework.generics import DestroyAPIView
from rest_framework.permissions import IsAuthenticated,IsAdminUser,AllowAny,IsAuthenticatedOrReadOnly
# Create your views here.



# fuction based views CRUD operation

@api_view(['GET','POST'])
def get_and_post(request):
    if request.method == 'GET':
        data = Blog.objects.all()
        serializer = BlogSerializer(data,many=True)
        return Response(serializer.data)
    elif request.method == 'POST':
        serializer = BlogSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors)
    
@api_view(['PUT','PATCH','GET'])  
def put_and_patch(request,pk):
    data = Blog.objects.get(pk=pk)
    serializer = BlogSerializer(data,data=request.data,partial=True)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data)
    return Response(serializer.errors)

@api_view(['GET','DELETE'])
def delete(request,pk):
    data = Blog.objects.get(pk=pk)
    data.delete()
    return Response('deleted')




# class based views CRUD operations

class ListApiView(ListAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
    permission_classes = [IsAdminUser]

class CreateApiView(CreateAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer

class RetrieveApiView(RetrieveAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
    permission_classes = [IsAuthenticated]

class UpdateApiView(UpdateAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
  

class DestroyApiView(DestroyAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
