from django.urls import path
from apiapp import views


urlpatterns = [
    
    path('',views.get_and_post,name='get'),
    path('put/<int:pk>',views.put_and_patch,name='put'),
    path('delete/<int:pk>',views.delete,name='delete'),
    path('list',views.ListApiView.as_view(),name="ListAPIView"),
    path('create',views.CreateApiView.as_view(),name="CreateAPIView"),
    path('retrieve/<int:pk>',views.RetrieveApiView.as_view(),name="RetrieveAPIView"),
    path('update/<int:pk>',views.UpdateApiView.as_view(),name="UpdateAPIView"),
    path('destroy/<int:pk>',views.DestroyApiView.as_view(),name="DestroyAPIView"),
]
