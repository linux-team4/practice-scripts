from django.urls import path,include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()

router.register(r'blog-crud', views.Modelview,basename='blog-crud')


urlpatterns = [
    
    path('',include(router.urls)),
    path('create',views.Create.as_view()),
  
]  
