from django.shortcuts import render
from rest_framework import viewsets
from .models import Blog
from .serializers import BlogSerializer
from rest_framework import generics

# Create your views here.

class Modelview(viewsets.ModelViewSet):

    queryset = Blog.objects.all()
    serializer_class = BlogSerializer

class Create(generics.CreateAPIView):
    queryset = Blog.objects.all()
    serializer_class = BlogSerializer
