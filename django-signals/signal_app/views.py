from django.shortcuts import render
from django.contrib.auth.models import User
from signal_app.models import Profile
# Create your views here.




def test_signals(request):
    
    user = User.objects.create_user(username='bala@viper', email='bala@gmail.com', password='password123')
  
    profile = Profile.objects.get(user=user)
    print(f"Profile created for user: {profile.user.username}")

    user.first_name = 'balachandran'
    user.last_name = 'baskaran'
    user.profile.phone = '6380688522'
    user.save()

   
    updated_profile = Profile.objects.get(user=user)
    print(f"Profile updated for user: {updated_profile.user.first_name}")

    return render(request, 'test_signal.html')



