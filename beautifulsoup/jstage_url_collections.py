import os
import io
import gzip
import requests
import sys
from bs4 import BeautifulSoup

def scrap_links(url):
    Response = requests.get(url)
    soup = BeautifulSoup(Response.content, 'lxml')

    volume_links = []
    for vlinks in soup.find_all('a', class_="bluelink-style vol-link"):
        volume_links.append(vlinks.get('href'))

    issues=[]
    with open('links.txt', 'w') as file:
        for ilinks in soup.find_all('a', class_="bluelink-style customTooltip"):
            link = ilinks.get("href")
            issues.append(ilinks["href"])

            if link and link.startswith("https://www.jstage.jst.go.jp/article/jamdsm/"):
                file.write(f"{link}\n")
        

    with open("links.txt", "a") as file:
        for issue_link in issues:
            article_response = requests.get(issue_link)
            article_soup = BeautifulSoup(article_response.content, "lxml")
            for alink in article_soup.find_all('a', class_="bluelink-style customTooltip"):
                article = alink["href"]
                if article.startswith("https://www.jstage.jst.go.jp/article/jamdsm/"):
                    file.write(f"{article}\n")
                    

    print("Links saved to links.txt")

def gzip_collection(url_file_path, target_path, agent=None):
    if not os.path.exists(target_path):
        os.makedirs(target_path)

    # read urls from the text file.................
    with open(url_file_path, 'r') as url_file:
        urls = url_file.read().splitlines()

    for url in urls:    #iterate each url....
        url_get3 = requests.get(url, headers=agent, timeout=120)
        name_url = url_get3.url
        name = name_url.replace('/', '-')
        url3 = url_get3.text

        soup3 = BeautifulSoup(url3, 'lxml')
        outfilename = os.path.join(target_path, "{}.html.gz".format(name))

        with gzip.open(outfilename, 'wb') as output:
            with io.TextIOWrapper(output, encoding='utf-8') as enc:
                enc.write(soup3.prettify())
                print(outfilename, 'contains', os.stat(outfilename).st_size, 'bytes')
                os.system('file -b --mime {}'.format(outfilename))
                print("\n" + ('-' * 80) + "\n")


url_file_path = '/home/balachandran/projects/practice-day-1/sampleurl.txt'
target_path = 'output/html/jstage/' 
agent = {'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36"}  # Specify your User-Agent header


if __name__ == "__main__":

    url = sys.argv[1]
    # scrap_links(url)
    gzip_collection(url_file_path, target_path, agent)

    

    
    