from bs4 import BeautifulSoup
import requests
import gzip
import sys
import os
import json
from citation import Citation as citedata
from almamater.almamater import alma_out as mainfun
from almamater.almamater import JsonAlmamater as cf

from random_useragent.random_useragent import Randomize
r_agent_agent = Randomize()
rm_agent = r_agent_agent.random_agent('desktop', 'linux')
agent = {"User-Agent": rm_agent}

"""agent = {"User-Agent":'Mozilla/5.0  \
    (Windows NT 6.3; WOW64) AppleWebKit/537.36  \
    (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'}"""


if not os.path.exists("out/json/jstage/"):
    os.makedirs("out/json/jstage/")



class Jstage:

    def journal_title(soup):
        name = soup.find('meta',attrs={"name":"citation_journal_title"})['content']
        return name 

    def title(soup):
        
        titles = soup.find('meta',attrs={"name":"title"})
        title_meta = titles['content']
        return title_meta

    def abstract(soup):
        
        abstract_text = soup.find('meta',attrs={"name":"abstract"})['content']
        return abstract_text

    def keywords(soup):
        
        keywords_div = soup.find('div', class_='global-para')
        keywords_text = "".join(keyword.strip() for keyword in keywords_div.stripped_strings)[9:]
        return keywords_text

    def access_type(soup):
        
        access_text = soup.find('meta', attrs={"name":"access_control"})['content']
        return access_text

    def journal_issn(soup): 
        
        issn = soup.find('meta',attrs={"name":"citation_issn"})['content']
        return issn

    def journal_dictionary(soup):
        '''
        This function used for have to add additional
        key-values to output json
        '''
        issn = Jstage.journal_issn(soup)

        if issn != "":

            with open('data/journal_update_data.json', 'r') as f:
                distros_dict = json.load(f)

            issn_out = {"journal_name": "", "abbreviation": "",
                        "language": "", "editor": "", "former_names": "",
                        "history": "", "publisher": "", "open_access": "",
                        "impact_factor": "", "frequency": "", "iso": "",
                        "coden": "", "print_issn": "", "online_issn": "",
                        "lccn": "", "journal_page": "", "online_archive": "",
                        "online_access": "", "journal_logo": ""}

            for distro in distros_dict:
                print_issn = distro['print_issn']
                online_issn = distro['online_issn']

                if print_issn != "" or online_issn != "":

                    if (print_issn.strip() == issn.strip()
                       or online_issn.strip() == issn.strip()):
                        issn_out = distro

                else:
                    issn_out = {"journal_name": "", "abbreviation": "",
                                "language": "", "editor": "",
                                "former_names": "",
                                "history": "", "publisher": "",
                                "open_access": "",
                                "impact_factor": "", "frequency": "",
                                "iso": "",
                                "coden": "", "print_issn": "",
                                "online_issn": "",
                                "lccn": "",
                                "journal_page": "",
                                "online_archive": "",
                                "online_access": "", "journal_logo": ""}
            return issn_out

        else:
            issn_out = {"journal_name": "", "abbreviation": "",
                        "language": "", "editor": "", "former_names": "",
                        "history": "", "publisher": "", "open_access": "",
                        "impact_factor": "", "frequency": "", "iso": "",
                        "coden": "", "print_issn": "", "online_issn": "",
                        "lccn": "", "journal_page": "", "online_archive": "",
                        "online_access": "", "journal_logo": ""}
            return issn_out

    def impact_factor(soup):
        '''
        This function used for parse the
        impact_factor key
        '''
        val = Jstage.journal_dictionary(soup)
        impact_factor_count = ""

        if "impact_factor" in val and val["impact_factor"] != "":
            impact_factor_count = val["impact_factor"]

        else:
            impact_factor_count = "0.0"

        return impact_factor_count

    def corresponding_author(soup):
        corres = {}
        corres['name'] = ""
        corres['email'] = ""
        return corres

    def doi(soup):
        doi = soup.find('meta',attrs={"name":"citation_doi"})['content']
        return doi
    
    def pdf_link(soup):
    
        pdf = soup.find('meta',attrs={"name":"citation_pdf_url"})['content']
        return pdf

    def html_link(soup):
    
        html = soup.find('meta',attrs={"name":"og:url"})['content']
        return html

    def volume(soup):
        vol_ume = soup.find('meta',attrs={"name":"volume"})['content']
        return vol_ume

    def issue(soup):
        
        is_sue = soup.find('meta',attrs={"name":"issue"})['content']
        return is_sue

    def page_number(soup):
        
        pa_ges = soup.find('p',class_="global-para").text
        pages_text = pa_ges.split()[-1]
        return pages_text

   
    def author_institute(soup):
        author_list=[]
        print("author")

        if soup.find_all("meta",attrs={"name":"citation_author"}):

            for meta in soup.find_all("meta",attrs={"name":"citation_author"}):
                author = meta['content']
                print(author)
                author_name={}
                author_name['surname']=""

                author_name['given_name'] = author
                author_name['degree'] = ""

                author_name['email'] = ""

                author_name['orcid'] = ""

                author_name['institute']=[]
                for inst in soup.find_all('p',attrs={"class":"accordion_affilinfo"}):
                    name = inst.get_text()
                institute={}
                institute['apid']      = ""
                institute['federation']= ""
                institute['acronym']   = ""
                institute['address']   = ""
                institute['city']      = ""
                institute['district']  = ""
                institute['state']     = ""
                institute['country']   = ""
                institute['name']      = cf.remove_whitespace(name).strip()
                author_name['institute'].append(mainfun(institute['name']))
                author_list.append(author_name)
        return author_list


    def received_date(soup):
        
        date = soup.find('meta',attrs={"name":"citation_publication_date"})['content']
        return date

    def publication_date(soup):
        
        pb_date = soup.find('meta',attrs = {"name":"citation_online_date"})['content']
        return pb_date

def jstage(url):
    
    soup = BeautifulSoup(url, 'lxml')
    doi  = Jstage.doi(soup)
    if doi !=None:
        cite = citedata.cit_out(doi)
        
    else:
        cite=""

    if cite !=None:
        citation_val = str(cite['cite_count'])

    else:
        citation_val="0"

    

    article={}
    article['pub_type']                = "Journal Article"
    article['access_type']             = Jstage.access_type(soup)
    article['journal']                 = Jstage.journal_title(soup)
    article['journal_issn']            = Jstage.journal_issn(soup)
    article['journal_dictionary']      = Jstage.journal_dictionary(soup)
    article['impact_factor']           = Jstage.impact_factor(soup)

    article['citation_details']        = cite

    if citation_val != None:
        article['citation_count']      = citation_val

    else:
        article['citation_count']      = "0"
    
    article['doi']                     = Jstage.doi(soup)
    article['pmid']                    = ""
    article['pmc']                     = ""
    article['title']                   = Jstage.title(soup)
    article['authors']                 = Jstage.author_institute(soup)
    article['corresponding_author']    = Jstage.corresponding_author(soup)
    article['volume']                  = Jstage.volume(soup)
    article['issue']                   = Jstage.issue(soup)
    article['pagenum']                 = Jstage.page_number(soup)
    article['date_received']           = Jstage.received_date(soup)+"/01/01"
    article['date_accepted']           = Jstage.publication_date(soup)
    article['date_published']          = Jstage.publication_date(soup)
    article['abstract']                = Jstage.abstract(soup)
    article['keywords']                = Jstage.keywords(soup)
    article['pdf_url']                 = Jstage.pdf_link(soup)
    article['html_url']                = Jstage.html_link(soup)

    return article

if __name__ == "__main__":
    '''
    this function invoked only when used as a standalone script
    otherwise Article class and it's methods and other functions
    can be imported and used like any other standared module.
    '''
    url_input = sys.argv[1]
    print("url_input :: ", url_input)

    if ".html.gz" in url_input:
        with gzip.open(url_input, 'rb') as f:
            file_content = f.read()
            url = file_content.decode('utf-8')

    else:
        files = requests.get(url_input, headers=agent, timeout=60, verify=False)
        url = files.text

    result = jstage(url)
    doi = result['doi']
    print("doi",len(doi))
    if len(doi) > 5:
        doi = doi.replace("/", "_")
        output = "out/json/jstage/" + doi + '.json'
        print("Writing...!", output)

        with open(output, 'w') as fp:
            json.dump(result, fp)

    else:
        pass



    
