from django.urls import path
from my_app.views import trigger_task

urlpatterns = [
    path('trigger/', trigger_task, name='trigger_task'),
]