from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from my_app.tasks import add

def trigger_task(request):
    result = add.delay(4, 4)
    return HttpResponse(f"Task ID: {result.task_id}")
