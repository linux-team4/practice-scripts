
//alert("hi hello");

//alert(4 + 8);

console.log("hi i am building javascript")

// variables


let name1 = "bala";            // u can change same vaiable value once we use let keyword

console.log(name1)

let num1 = 45
let num2 = 56
let result = num1 + num2
console.log(result)

//const vaiables

const name2 = "balachandran"    // u cant change the value once u declared const keyword
console.log(name2)

const name3 = "balaviper"
const dob = "26/06/1998"
let msg = "hi my name is " + name3 + " and my dob is " + dob

console.log(msg)

// data types

// let age = 24;
// let byte = "five bytes"
// let bash = false
// let value = undefined
// let cash = null

// objects

let person = {
    Name : "bala",
    age : 24,
    gender : "Male",
    isalive : true,
    address : "2/19 kudi street, jambumadai",
    siblings : {
        brother : "partha",
        father :"baskaran",
        mother : "santhi"
    }
}

console.log(person)                 // print objects
console.log(person.Name)            // dot access by using key
console.log(person['age'])          // bracket notation

person.Name = "partha"              // change the values

console.log(person['Name'])

console.log(person.siblings)         // sub object
console.log(person.siblings.mother)

//  array

let favcolor = ["red","green","black"]    // array declaration
console.log(favcolor)

favcolor[4] = 25                          // value add

console.log(favcolor);

console.log(favcolor[2])                  // get particular value

console.log(favcolor.length);             // find length of an array

// functions

function users(){

    let name = "bala"
    console.log("name is "+ name)
       
}

users()

function user1(firstname,lastname){

    let msg = "hello "+ firstname+ " " + lastname ;
    console.log(msg)
       
}

user1("bala","viper")

function add(num1,num2){

    let count = "the total  is "+ " " + (num1 + num2)
    console.log(count)
}

add(50,50)

// operators

let x = 23 
let y = 50      // assignment operator

x += 5
y -= 10

console.log(x);
console.log(y);

let a = 52            // comparition operator

console.log(a > 90)
console.log(a < 100)

let dull = 10;        // arithmatic operator
let doll = 20

console.log(dull+doll);
console.log(dull-doll);
console.log(dull*doll);
console.log(dull/doll);

console.log(1 === 1);
console.log('1' === 1);     // string equality operator

console.log(1==1);
console.log('1' == '1');     // lose equality operator


//terinary operator

let age = 25;

let type = age > 18 ? "adult ticket" : "child ticket"

console.log(type)

let cost = 63;

let costs = cost < 50 ? "sell" : "not sell"

console.log(costs);

// logical oprators

console.log(true  && false);
console.log(true || false);

let ciblescore = true
let highincome = false

let eligibleperson = ciblescore && highincome

let applicationstatus = !eligibleperson

console.log("status :" + eligibleperson);

console.log("loanstatus :" + applicationstatus)

//logical opretor with boolean

let usercolor = 0;
let defaultcolor = "black"

let selective = usercolor||defaultcolor

console.log("selectcolor :"+selective)

// conditional statement

let condition = true;

if(condition){

    console.log("condtion : true")
    console.log("condition : false");
}
else{
    console.log("condition : false")
}

let weather = "hot";

if(weather=="hot"){

    console.log("weather is hot")
}
else{

    console.log("weather is cold")
}

// let israining = true;
// let iscloudy = false;

// if(israining || iscloudy){

//     console.log("dont forget to take umbrella ")
// }
// else{

//     console.log("sky is clear")
// }

let israining = false;
let iscloudy = false;

if(israining && iscloudy){

    console.log("dont forget to take umbrella ")
}
else{

    console.log("sky is clear")
}

// let hour = 19;

// if(hour >= 0 && hour <= 13){

//     console.log("good morning")
// }

// else if(hour >= 13 && hour <= 17){

//     console.log("good afternoon")
// }

// else if(hour >= 17 && hour <= 18){
//     console.log("good evening")
// }
// else{
//     console.log("good night")
// }

let hrs = new Date();
let hour = hrs.getHours();

if(hour >= 0 && hour <= 13){

    console.log("good morning")
}

else if(hour >= 13 && hour <= 17){

    console.log("good afternoon")
}

else if(hour >= 17 && hour <= 18){
    console.log("good evening")
}
else{
    console.log("good night")
}

// for loop

for(let i = 0; i<=5; i++){
    console.log(i)
}

for(let i = 0; i<=5; i++){

    if(i % 2 !==0){
        console.log(i)
    }
    
}

for(let i = 0; i<=5; i++){

    if(i % 2 !==1){
        console.log(i)
    }
    
}

for(let i = 0; i<=50; i++){

    if(i % 2 !==1){
        console.log(i)
    }
    
}

// while loop

let i = 0;

while(i<=7){
    if(i % 2 == 1){
        console.log(i)
    }

    i++
}


let j = 0;

while(j<=18){
    if(j % 2 == 0){
        console.log(j)
    }

    j++
}

// for in loop

const persons01 ={
    name : "bala",
    age  :   24,
    gender : "male"
}

for(let key in persons01){

    console.log(key + " :",persons01[key])
}

const color = ["red","blue","black"]

for(let keys in color){

    console.log(color[keys]);
}
                          
//for off loop

for(let colors of color){

    console.log("color :"+colors)
}


// functions------------------> oop concept

let people = {

    name : "balaviper",
    age : 24,
    gender : "male",
    others : {
        street : "lay street",
        doorno :  2/19,
    },
    greeting : function(){
          
        let msg = "hi im bala from trichy"
        console.log(msg)
    }
    

};

console.log(people.others)










